package consoleJavaApp.config;

import consoleJavaApp.model.User;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Configuration
@PropertySource(value="classpath:app.properties")
public class KafkaProducerConfig {
    @Autowired
    Environment env;

    @Bean
    public Properties producerProperties() {
        String propertyPath = env.getProperty("kafka.property.path");
        Properties prop = new Properties();
        try(InputStream inputStream = new FileInputStream(propertyPath)){
            prop.load(inputStream);
        } catch(IOException ioE) {
            ioE.printStackTrace();
            throw new IllegalArgumentException();
        }

        prop.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class);
        prop.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                JsonSerializer.class);
        return prop;
    }

    @Bean("kafkaProducer")
    public KafkaProducer<String, User> kafkaProducer() {
        return new KafkaProducer<>(producerProperties());
    }
}
