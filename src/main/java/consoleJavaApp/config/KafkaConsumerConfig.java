package consoleJavaApp.config;

import consoleJavaApp.model.User;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

@Configuration
@PropertySource(value="classpath:app.properties")
public class KafkaConsumerConfig {

    @Autowired
    Environment env;
    @Bean
    public Properties consumerProperties() {
        final String propertyPath = env.getProperty("kafka.property.path");

        Properties prop = new Properties();
        try (InputStream inputStream = new FileInputStream(propertyPath)) {
            prop.load(inputStream);
        } catch (IOException ioE) {
            ioE.printStackTrace();
            throw new IllegalArgumentException();
        }
        prop.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        prop.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        prop.put(JsonDeserializer.VALUE_DEFAULT_TYPE, User.class);
        prop.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG,env.getProperty("users.count"));
        return prop;
    }

    @Bean("kafkaConsumer")
    public KafkaConsumer<String, User> kafkaConsumer() {
        KafkaConsumer<String, User> kafkaConsumer = new KafkaConsumer<>(consumerProperties());
        kafkaConsumer.subscribe(Arrays.asList(env.getProperty("consumer.topics").split("[,]+")));
        return kafkaConsumer;
    }
}