package consoleJavaApp.service;

import consoleJavaApp.model.User;
import java.util.List;

public interface ObjService {
    void addToFirstTable(int usersCount);
    void addToSecondTable(List<User> user);
    void addToSecondTable(int userCount);
    List<User> getListFromFirstTable();
    List<User> getListFromSecondTable();
}
