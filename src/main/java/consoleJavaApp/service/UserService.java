package consoleJavaApp.service;

import consoleJavaApp.dao.Dao;
import consoleJavaApp.model.DefaultUserEntity;
import consoleJavaApp.model.KafkaUserEntity;
import consoleJavaApp.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalTime;
import java.util.List;

@Service("userService")
public class UserService implements ObjService {

    private Dao dao;
    Logger log = LoggerFactory.getLogger(UserService.class);
    private static int counter;

    @Autowired
    public UserService(Dao dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public void addToFirstTable(int usersCount){
        for(int i=0;i<usersCount;++i){
            add(new DefaultUserEntity("Name-"+i
                    ,new Timestamp(System.currentTimeMillis())));
        }
    }

    private void add(User user) {
        int c = counter;
        log.debug("Transaction-{} is going...",c);
        dao.add(user);
        log.debug("Transaction-{} is done: {} has been saved as {} at {}",c,user,user.getClass().getSimpleName() , LocalTime.now());
        counter++;
    }

    @Override
    @Transactional
    public void addToSecondTable(int usersCount) {
        for(int i=0;i<usersCount;++i){
            add(new KafkaUserEntity("Name-"+i
                    ,new Timestamp(System.currentTimeMillis())));
        }
    }

    @Override
    @Transactional
    public void addToSecondTable(List<User> users) {
        for(User user:users){
           add(new KafkaUserEntity(user.getName(),user.getTimestamp()));
        }
    }

    @Override
    @Transactional
    public List<User> getListFromFirstTable() {
        int c = counter;
        log.debug("Transaction-{} is going...",c);
        List<User>  users = dao.getListFromFirstTable();
        log.debug("Transaction-{} is done: {} users was received from {} table at {}",c,users.size(), 1, LocalTime.now());
        counter++;
        return users;
    }

    @Override
    @Transactional
    public List<User> getListFromSecondTable() {
        int c = counter;
        log.debug("Transaction-{} is going...",c);
        List<User>  users = dao.getListFromSecondTable();
        log.debug("Transaction-{} is done: {} users was received from {} table at {}",c,users.size(), 2 ,LocalTime.now());
        counter++;
        return users;
    }
}
