package consoleJavaApp.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="kafka_users")
public class KafkaUserEntity extends User {
    public KafkaUserEntity() {}
    public KafkaUserEntity(String name, Timestamp timestamp) {
        super(name,timestamp);
    }
}

