package consoleJavaApp.model;

import java.sql.Timestamp;
import javax.persistence.*;

@Entity
@Table(name="default_users")
public class DefaultUserEntity extends User {
    public DefaultUserEntity() {}
    public DefaultUserEntity(String name, Timestamp timestamp) {
        super(name,timestamp);
    }
}

