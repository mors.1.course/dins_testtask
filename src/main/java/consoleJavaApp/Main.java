package consoleJavaApp;

import consoleJavaApp.config.JpaConfig;
import consoleJavaApp.config.KafkaConsumerConfig;
import consoleJavaApp.config.KafkaProducerConfig;
import consoleJavaApp.model.User;
import consoleJavaApp.service.ObjService;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("Application has been started");
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(JpaConfig.class, KafkaProducerConfig.class, KafkaConsumerConfig.class);

        Environment env = context.getEnvironment();
        final String PRODUCER_TOPIC = env.getProperty("producer.topic");
        final int USERS_COUNT = Integer.parseInt(env.getProperty("users.count"));

//        System.out.println(Arrays.asList(context.getBeanDefinitionNames()));

        ObjService userService = context.getBean("userService",ObjService.class);
        Producer<String, User> producer =context.getBean("kafkaProducer", KafkaProducer.class);
        Consumer<String, User> consumer = context.getBean("kafkaConsumer",KafkaConsumer.class);
        context.registerShutdownHook();

        System.out.print(
                "Type application mode:\n" +
                "----------\n" +
                "mode-1 : Read the data from the first database table, and write JSON messages to the topic.\n" +
                "mode-2 : Get all messages from the topic and write them into the second table.\n" +
                "exit : To stop application\n" +
                "Users count:" + env.getProperty("users.count") +
                "\n----------\n"
        );
        userService.addToFirstTable(USERS_COUNT);
        int firstTableUsersCounter=USERS_COUNT;
        int secondTableUsersCounter=0;
        int producedMessages=0;
        int receivedMessages=0;
        Scanner scanner = new Scanner(System.in);
        String mode = "start";
        List<User> firstTableUsers=null;
        while (!mode.equals("exit")){
            System.out.println("First table contains:"+firstTableUsersCounter +" users");
            System.out.println("Second table contains:"+secondTableUsersCounter+ " users");
            System.out.println("Produced messages:"+producedMessages);
            System.out.println("Received messages:"+receivedMessages);
            System.out.print(">");
            mode = scanner.next();
            switch (mode) {
                case "mode-1" -> {
                    logger.info("Mode-1 is activated");
                    firstTableUsers=userService.getListFromFirstTable();
                    KafkaServer.sendMessages(producer, PRODUCER_TOPIC, firstTableUsers);
                    producedMessages+=firstTableUsers.size();
                }
                case "mode-2" -> {
                    if(producedMessages==receivedMessages){
                        System.out.println("(!) Mode-1 sent no one new message");
                        break;
                    }
                    logger.info("Mode-2 is activated");
                    List<User> kafkaUsers = KafkaServer.getMessages(consumer, USERS_COUNT);
                    receivedMessages+=kafkaUsers.size();
                    userService.addToSecondTable(kafkaUsers);
                    secondTableUsersCounter=receivedMessages;
                }
                case "exit" -> {
                    System.out.println("Log will be saved into /logging/logging.log");
                }
                default -> System.out.println("(!) Wrong sequences, use allowed [mode-1/mode-2/exit] keywords");
            }
        }
        producer.close();
        logger.debug("Producer {} is closed",producer.getClass().getSimpleName());
        consumer.close();
        logger.debug("Consumer {} is closed",consumer.getClass().getSimpleName());
        logger.info("Application has been finished");
    }

    private static class KafkaServer{
        public static void sendMessages(Producer<String,User> producer, String topic, List<? extends User> users){
            try{
                for(int i=0; i<users.size();i++){
                    logger.debug("Trying to produce a record({},{}) into topic-{}...",i,users.get(i),topic);
                    producer.send(new ProducerRecord<>(topic, String.valueOf(i), users.get(i))
                            , (metadata, exception) -> {
                                if (exception != null) {
                                    logger.error(exception.getMessage());
                                } else {
                                    LocalTime time = new Timestamp(metadata.timestamp()).toLocalDateTime().toLocalTime();
                                    logger.debug("Produced record at {} to topic {} into partition-{}, offset-{}"
                                            , time, metadata.topic(), metadata.partition(), metadata.offset());
                                }
                            });
                }
            } finally {
                producer.flush();
            }
        }
        public static List<User> getMessages(Consumer<String,User> consumer,int count){
            List<User> users = new ArrayList<>();
            int receivedMsgCounter=0;

            waiting:
            while (receivedMsgCounter<count) {
                logger.debug("Consumer {} is waiting for messages...",consumer.getClass().getSimpleName());
                ConsumerRecords<String, User> records = consumer.poll(Duration.ofSeconds(10));
                for (ConsumerRecord<String, User> record : records) {
                    logger.debug("Received record({},{}) at {} from topic {}, partition-{}, offset-{}",
                            record.key(), record.value(), LocalTime.now(), record.topic(), record.partition(), record.offset());
                    users.add(record.value());
                    receivedMsgCounter++;
                }
            }
            return users;
        }
    }
}
