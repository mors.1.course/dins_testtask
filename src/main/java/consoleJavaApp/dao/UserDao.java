package consoleJavaApp.dao;

import consoleJavaApp.model.User;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
@Repository
public class UserDao implements Dao{

    @PersistenceContext(unitName = "entityManagerFactory")
    private EntityManager entityManager;

    @Override
    public void add(User user) {
        entityManager.persist(user);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getListFromFirstTable() {
        return entityManager.createQuery("from DefaultUserEntity").getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getListFromSecondTable() {
        return entityManager.createQuery("from KafkaUserEntity").getResultList();
    }
}
