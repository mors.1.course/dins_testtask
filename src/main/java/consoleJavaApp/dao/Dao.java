package consoleJavaApp.dao;

import consoleJavaApp.model.User;

import java.util.List;

public interface Dao {
    void add(User user);
    List<User> getListFromFirstTable();
    List<User> getListFromSecondTable();
}
